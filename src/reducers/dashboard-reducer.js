const initialState = {
    state_task: [{group_id: '', category_id: '',  task: []}],
    groups: [{ id:'-', name: '-', description: '-'}],
    group_data: [],
    graph: [{ name: '', count: ''}],
    graphg: [{ name: '', count: '' }],
    notes: [{ name: '', content: ''}],
    current_group_id: '-',
    current_note_id: '-',
    current_note_name: '-',
    current_note_content: '-',
    group_invitations: [],
    group_members: []
  };
  
  export default function dashboardReducer(state = initialState, action) {
    switch (action.type) {
/*       case "SET_TASKS": {
        let index = 0;
        let aux = 0;
        while (index < state.state_task.length) {
          aux = index;
          if (state.state_task[index].category_id == '') {
            state.state_task[index] = action.payload;
            console.log("Novo estado")
            console.log(state.state_task)
            return state;
          }
          if (action.payload.category_id == state.state_task[index].category_id) {
            state.state_task[index] = action.payload;

            console.log("Novo estado")
            console.log(state.state_task)
            return state;
          }
          if (aux == (state.state_task.length - 1) && action.payload.category_id != state.state_task[index].category_id){
            const newState = { ...state, state_task: [...state.state_task, action.payload] };
            console.log("Novo estado")
            console.log(newState)
            return newState;
          }
          else{
            index++;
          }
        }
      } */
      case "SET_GROUPS": {
        const newState = { ...state, current_group_id: action.payload[0].id, groups: action.payload};
        return newState;
      }
      case "SET_GROUP_DATA":{
        console.log("aqui*");
        const newState = { ...state, group_data: action.payload };
        return newState;
      }
      case "INSERT_GRAPH": {
        const newState = { ...state, graph: action.payload };
        return newState;
      }
      case "INSERT_GRAPHH": {
        const newState = { ...state, graphg: action.payload };
        return newState;
      }
      case "SET_NOTES": {
        const newState = { ...state, notes: action.payload };
        return newState;
      }
      case "SET_CURRENT_GROUP_ID": {
        const newState = { ...state, current_group_id: action.payload.id };
        return newState;
      }
      case "SET_CURRENT_NOTE": {
        const newState = { ...state, current_note_name: action.payload.current_note_name, current_note_content: action.payload.current_note_content, current_note_id: action.payload.current_note_id };
        return newState;
      }
      case "SET_GROUP_INVITATOINS":{
        const newState = { ...state, group_invitations: action.payload};
        return newState;
      }
      case "SET_GROUP_MEMBERS":{
        const newState = { ...state, group_members: action.payload};
        return newState;
      }

      case "SET_STATE_NULL":{
        const newState = {state_task: [{group_id: '', category_id: '',  task: []}],
                          groups: [{ id:'-', name: '-', description: '-'}],
                          group_data: [],
                          graph: [{ name: '', count: ''}],
                          graphg: [{ name: '', count: '' }],
                          notes: [{ name: '', content: ''}],
                          current_group_id: '-',
                          current_note_id: '-',
                          current_note_name: '-',
                          current_note_content: '-',
                          group_invitations: [],
                          group_members: []}
        return newState;
      }

      default: {
        return state;
      }
    }
  }