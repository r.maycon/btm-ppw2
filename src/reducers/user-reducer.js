const initialState = {
    user_data: {user_token: "", id: "", name: '', email: ''},
    closeModal: false
  };

  export default function userReducer(state = initialState, action) {
    switch (action.type) {
      case "SET_USERDATA": {
        const newState = { ...state, user_data: action.payload };
        return newState;
      }
      case "CLOSE_MODAL": {
        const newState = { ...state, closeModal: true };
        return newState;
      }
      default: {
        return state;
      }
    }
  }