<addGroup>
  <div class="modal fade" id="addGroup">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
        <div class="modal-header">
          <h4 class="modal-title">Criar Grupo</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nomeGrupo">Nome do Grupo:</label>
            <p><input type="text" class="form-control" id="nomeGrupo" placeholder="Digite um nome para o grupo."></p> 
            <label for="descriptionGroup">Descrição: </label>
            <p><textarea id="descriptionGroup" name="description" class="form-control" placeholder="Descrição do grupo"></textarea></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick={this.newGroup}>Criar</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
        </div>
      </div>
    </div>
  </div>

 <script>
    this.mixin("state")
    this.mixin("reduxGlobal")
    this.mixin('subscribeStateTag')
    this.subscribeStateTag(this)

   newGroup(){

    let new_group = { name: document.getElementById('nomeGrupo').value, description: document.getElementById('descriptionGroup').value };
    let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id }

    this.dispatch({
       type: "ADD_GROUP",
         payload: {
           new_group,
           userinfo
         }
    });
   }

    this.on('update', function() {
        if(this.state.userReducer.closeModal){
            $('#addGroup').modal('hide');
        }
    })
 </script>

 <style>
    #spinner{
        position: absolute;
        width: 100%;
        z-index: 10;
        text-align: center;
        height: 100%;
        background: rgba(255, 255, 255, 0.7);
        padding-top: 7%;
    }
 </style>
</addGroup>