<addTask>
    <div class="modal fade" id="addTask">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Nova Tarefa</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="nome_tarefa">Qual a sua tarefa:</label>
                                <input type="text" class="form-control" id="task_name" placeholder="Digite sua tarefa">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-12 text-left">
                                <label for="description">Descrição: </label>
                                <textarea id="description" name="description" class="form-control" placeholder="Descreva sua tarefa..." required="required"></textarea>
                            </div>
                        </div><br>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick={this.newTask}>Criar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        newTask(){

            if(document.getElementById('task_name').value != "" && document.getElementById('description').value != ""){
                let new_task = { name: document.getElementById('task_name').value, description: document.getElementById('description').value, private: false };
                let userinfo = { token: this.state.userReducer.user_data.user_token };
                let group = { id: this.state.dashboardReducer.current_group_id };
                let category = { id: opts.category_id };

                this.dispatch({
                    type: "ADD_TASK",
                    payload: {
                        new_task,
                        group,
                        category,
                        userinfo
                    }
                });
            }else{
                alert("Preecha todos os campos!");
            }
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#addTask').modal('hide');
            }
        })
    </script>

    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>
</addTask>