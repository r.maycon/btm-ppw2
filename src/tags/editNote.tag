<editNote>
    <div class="modal fade" id="editNote">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Editar Nota</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label for="note_title">Titulo da nota</label>
                    <p><input type="text" class="form-control" id="editnote_title" value="{this.state.dashboardReducer.current_note_name}" placeholder="{this.state.dashboardReducer.current_note_name}"></p> 
                    <label for="note_content">Descrição: </label>
                    <p><textarea id="editnote_content" name="description" class="form-control" value="{this.state.dashboardReducer.current_note_content}" placeholder="{this.state.dashboardReducer.current_note_content}"></textarea></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick={deletarNota}>Excluir Nota</button>
                    <button type="button" class="btn btn-success" onclick={editarNota}>Editar Nota</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        editarNota(){
            let note = { id: this.state.dashboardReducer.current_note_id, name: document.getElementById('editnote_title').value, content: document.getElementById('editnote_content').value };
            let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id };
            let group = { id: this.state.dashboardReducer.current_group_id };
            this.dispatch({
                type: 'EDIT_NOTE',
                payload: {
                    note,
                    userinfo,
                    group
                }
            });
            $('#editNote').modal('hide');
            $('#note').modal('show');
        }

        deletarNota(){
            let note = { id: this.state.dashboardReducer.current_note_id }
            let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id };
            let group = { id: this.state.dashboardReducer.current_group_id };

            this.dispatch({
                type: 'DELETE_NOTE',
                payload: {
                    note,
                    userinfo,
                    group
                }
            });
            $('#editNote').modal('hide');
            $('#note').modal('show');
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#editNote').modal('hide');
            }
        })
    </script>

    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>
</editNote>