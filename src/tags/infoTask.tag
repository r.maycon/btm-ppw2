<infoTask>
    <div class="modal fade" id="infoTask">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title text-center">{opts.name}</h4>
                    <a if={this.state.dashboardReducer.group_data.isOwner} class="btn-icon" onclick={edit} title="Editar"><img src="imgs/edit.svg" width="20"></a>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <p class="text-center">{opts.description}</p>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        edit(){
            $('#editTask').modal('show');
            $('#infoTask').modal('hide');
        }
    
    </script>
    <style>
     #infoTarefa .modal-header, #infoTarefa .modal-body{
         background: #ff8;
     }
    </style>
</infoTask>