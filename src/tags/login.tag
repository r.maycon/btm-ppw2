<login>
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="entrarTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" if={this.state.exibicaoReducer.showError}>
                    <strong>Ops!</strong> {this.state.exibicaoReducer.msgError}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <form>
                    <div class="modal-header">
                        <h5 class="modal-title" id="entrarTitle">Entrar</h5>
                    </div>
                    <div class="modal-body">
                        <p><input autofocus="autofocus" value="" name="email" id="email" type="email" placeholder="E-mail" class="form-control" required="required"></p>
                        <p><input autocomplete="off" name="password" id="password" type="password" placeholder="Senha" class="form-control" required="required"></p>
                    </div>
                    <div class="modal-footer">
                        <ul>
                            <li><input type="button" name="commit" value="Entrar" class="btn btn-success btn-fill btn-block" data-disable-with="Entrar" onclick={this.login}></li>
                            <li><a class="btn btn-block" data-toggle="modal" data-target="#recuperar_senha">Recuperar senha</a></li>
                            <li><a class="btn btn-block" data-toggle="modal" data-target="#cadastro">Cadastre-se grátis</a></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        login(){
            let newLogin = {email: (document.getElementById('email').value), password: (document.getElementById('password').value)};

            this.dispatch({
                type: "LOGIN",
                payload: {
                    newLogin
                }
            });
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#login').modal('hide');
            }
        })

    </script>

    <style>
        a{
            cursor: pointer;
        }
        #login .modal-footer ul{
            list-style: none;
            padding: 0;
            width: 100%;
        }
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 15%;
        }
    </style>
</login>