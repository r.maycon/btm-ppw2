<addCategory>
    <div class="modal fade" id="addCategory">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Cadastrar Categoria</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nomeCategoria">Nome:</label>
                        <input type="text" class="form-control" id="nomeCategoria" placeholder="Digite o nome da categoria">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick={newCategory}>Criar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        newCategory(){
            let category = { name: document.getElementById('nomeCategoria').value, description: '' };
            let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id };
            let group = { id: this.state.dashboardReducer.current_group_id };
            console.log(this.state.dashboardReducer.current_group_id)
            this.dispatch({
                type: "ADD_CATEGORY",
                payload: {
                    category,
                    userinfo,
                    group
                }
            });
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#addCategory').modal('hide');
            }
        })
    </script>
    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>
</addCategory>