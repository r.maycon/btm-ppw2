<app>
    <style>
    </style>
    <home if='{state.exibicaoReducer.home}'></home>
    <dashboard if='{state.exibicaoReducer.dashboard}'></dashboard>
    <perfilUsuario if='{state.exibicaoReducer.perfilUsuario}'></perfilUsuario>
    <anotacoes if='{state.exibicaoReducer.anotacoes}'></anotacoes>
    <grafico2 if='{state.exibicaoReducer.grafico2}'></grafico2>
    <script>
        this.mixin('state')
        this.mixin('reduxGlobal')
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)
    </script>
</app>