<dashboard>
    <editUser></editUser>
    <addMember></addMember>
    <addGroup></addGroup>
    <notifications></notifications>
    <groupMembers></groupMembers>
    <editGroup group_id="{this.group_id}" group_name="{this.group_name}"></editGroup>
    <note></note>
    <addNote group_id="{this.group_id}"></addNote>
    <editNote group_id="{this.group_id}" title_edit="{this.state.dashboardReducer.current_note_name}" content_edit="{this.state.dashboardReducer.current_note_name}"></editNote>
    <addCategory group_id="{this.group_id}"></addCategory>
    <editCategory category_id="{this.category_id}" category_name="{this.category_name}" group_id="{this.group_id}"></editCategory>
    <addTask category_id="{this.category_id}"></addTask>
    <infoTask name="{this.show_task_name}" description="{this.show_task_description}" id="{this.show_task_id}" category_id="{this.show_category_id}"></infoTask>
    <editTask name="{this.show_task_name}" description="{this.show_task_description}" id="{this.show_task_id}" category_id="{this.show_category_id}"></editTask>
    <header></header>
    <section class="container-fluid" id="content">
        <div class="container" if={this.state.exibicaoReducer.showAlertMsg != ""}><br>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {this.state.exibicaoReducer.showAlertMsg}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        <div class="row panel-header" id="categories">
            <div class="col-md-4"><br>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Grupo atual</h4>
                    </div>
                    <div class="col-md-6" if={this.state.dashboardReducer.group_data.isOwner}>
                        <a href="" data-toggle="modal" data-target="#editGroup" class="group-edit float-right">Editar</a>
                    </div>
                    <div class="col-md-6" if={!this.state.dashboardReducer.group_data.isOwner}>
                        <a onclick={leaveGroup} class="group-logout float-right">Sair do grupo</a>
                    </div>
                </div>
                <select id="selectgroup" class="form-control" onchange="{setGroup}">
                    <option>Selecione um grupo</option>
                    <option each={ groups in this.state.dashboardReducer.groups } value="{groups.id}">{groups.name}</option>
                </select>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <ul class="dashboard-navbar-nav ml-auto">
                        <li if={this.state.dashboardReducer.group_data.isOwner}><a class="btn btn-outline-success" data-toggle="modal" data-target="#addMember"><img src="imgs/user.svg" width="22"> Adicionar Membro</a></li>
                        <li><a class="btn btn-outline-success" data-toggle="modal" data-target="#addGroup"><img src="imgs/category.svg" width="22"> Adicionar Grupo</a></li>
                        <li><a if={this.state.dashboardReducer.group_data.isOwner} data-toggle="modal" data-target="#addCategory" class="btn btn-outline-success"><img src="imgs/add-task.svg" width="22"> Adicionar Categoria</a></li>
                    </ul>
                </div>
                <div class="row">
                    <ul class="dashboard-navbar-nav ml-auto">
                        <li if={this.state.dashboardReducer.group_data.isOwner}><a class="btn btn-outline-success" onclick={getGroupMembers}><img src="imgs/multiple.svg" width="22"> Membros do Grupo</a></li>
                        <li><a class="btn btn-outline-info" data-toggle="modal" data-target="#note"><img src="imgs/review.svg" width="22"> Notas & Observações</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <div id="board">
            <div class="card group-category" each={ category in this.state.dashboardReducer.group_data.categories } >
                <div class="row card-header">
                    <div class="col-md-9">
                        <h4>{category.name}</h4>
                    </div>
                    <div class="col-md-3">
                        <a if={this.state.dashboardReducer.group_data.isOwner} class="btn-icon" onclick={setCategoryIdCategoryName.bind(this,category.id,category.name)} title="Editar"><img src="imgs/edit.svg" width="20"></a>
                        <a if={this.state.dashboardReducer.group_data.isOwner} class="btn-icon" onclick={deletarCategoria.bind(this,category.id)} title="Excluir"><img src="imgs/remove.svg" width="20"></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <a if={this.state.dashboardReducer.group_data.isOwner} href="javascript:void(0)" class="btn btn-success btn-block" onclick={addTask}>Adicionar Tarefa</a>
                            </li>
                            <li class="list-group-item" each={ task in category.tasks }>
                                <a class="task" onclick={showTask.bind(this,task.name,task.description,task.id,category.id)}>
                                    <p>{ task.name }</p>
                                    <span class="task-description">{ task.description }</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)
        
        this.groups = this.state.dashboardReducer.groups;
        this.tasks = this.state.dashboardReducer.tasks;
        let category_id = 0;
        let category_name = '';

        addTask(e){
            this.category_id = e.item.category.id;
            $('#addTask').modal('show');
        }

         setGroup(){
                let e = document.getElementById("selectgroup");
                var id = document.getElementById("selectgroup").value;

                let group2 = e.options[e.selectedIndex].text;

                this.group_name = group2;

                this.dispatch({
                    type: "SET_CURRENT_GROUP_ID",
                    payload: {
                        id
                    }
                });

                let data = { token: this.state.userReducer.user_data.user_token, group_id: this.state.dashboardReducer.current_group_id };

                this.dispatch({
                    type: "GET_GROUP",
                    payload: {
                        data
                    }
                });

                this.dispatch({
                    type: "GET_NOTES",
                    payload: {
                        data
                    }
                })
            }
        
        setCategoryIdCategoryName(category_id, category_name){
            this.category_id = category_id;
            this.category_name = category_name;
            $('#editCategory').modal('show');
        }

        showTask(name, description, id, id_category){
            this.show_task_name = name;
            this.show_task_description = description;
            this.show_task_id = id;
            this.show_category_id = id_category;
            $('#infoTask').modal('show');
        }

        deletarCategoria(category_id){
            let category = { id: category_id };
            let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id };
            let group = { id: this.state.dashboardReducer.current_group_id };
            this.dispatch(
                { type: 'DELETE_CATEGORY',
                    payload: {
                        category,
                        userinfo,
                        group
                }
            });
        }

        getGroupMembers(){
            $('#groupMembers').modal('show');
            this.dispatch(
                { type: 'GET_GROUP_MEMBERS',
                    payload: {
                        token: this.state.userReducer.user_data.user_token,
                        group_id: this.state.dashboardReducer.current_group_id
                }
            });
        }


        leaveGroup(){
            this.dispatch(
                { type: 'LEAVE_GROUP',
                    payload: {
                        token: this.state.userReducer.user_data.user_token,
                        group_id: this.state.dashboardReducer.current_group_id
                }
            });
        }
    </script>

    <style>    
        a{
            cursor: pointer;
        }
        .panel-header{
            background: #f7f7f7;
            padding: 10px;
        }
        .navbar-brand img{
            width: 150px;
        }
        #dashboard-menu{
            line-height: 32px;
        }
        .task{
            width: 100%;
            height: 100px;
            text-align: center;
            float: left;
            display: inline-block;
            vertical-align: top;
            padding: 10px;
            color: black;
            background: #ff8;
            text-align: center;
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            box-shadow: 2px 2px 2px #eee;
            line-height: 14px;
        }
        .task:hover{
            text-decoration: none;
        }
        .task-description{
            font-size: 10pt;
        }
        #board{
            overflow-x: auto;
            white-space: nowrap;
        }
        #board .col-md-12{
            padding: 0;
        }
        .group-category{
            width: 320px;
            vertical-align: top;
            margin-left: 15px;
            display: inline-block;
        }
        .card-header{
            background: #fff;
        }
        .dashboard-navbar-nav{
            list-style: none;
        }
        .dashboard-navbar-nav li{
            display: inline;
            margin-right: 10px;
        }
        .btn:hover{
            text-decoration: none;
        }
        .btn-icon{
            padding: 5px;
            cursor: pointer;
        }
        .group-edit{
            color: #5fcd4d;
            font-size: 9pt;
            font-weight: bold;
        }
        .group-logout{
            color: red;
            font-size: 9pt;
            font-weight: bold;
        }
    </style>

</dashboard>
