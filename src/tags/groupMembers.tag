<groupMembers>
    <div class="modal fade" id="groupMembers">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Membros do grupo {opts.group_name}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
    
                <div class="modal-body">
                    <div class="row-fluid" each={ member in this.state.dashboardReducer.group_members }>
                        <table class="table">
                            <tr>
                                <td>{member.name} ({member.email})</td>{member.id}
                                <td><a class="btn btn-outline-danger" onclick={removeMemberGroup.bind(this, member.id)}>Remover do grupo</a></td>
                            </tr>
                        </table>
                        <hr>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        acceptInvite(group_id){
            this.dispatch({   
                type: 'ACCEPT_INVITE',
                payload: {
                    token: this.state.userReducer.user_data.user_token,
                    id: group_id
                }
            });
        }

        rejectInvite(group_id){
            this.dispatch({   
                type: 'REJECT_INVITE',
                payload: {
                    id: group_id
                }
            });
        }

        removeMemberGroup(member_id){
            this.dispatch(
                { type: 'REMOVE_MEMBER_GROUP',
                    payload: {
                        token: this.state.userReducer.user_data.user_token,
                        member_id: member_id,
                        group_id: this.state.dashboardReducer.current_group_id
                }
            });
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#groupMembers').modal('hide');
            }
        })
    </script>

    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>
</groupMembers>