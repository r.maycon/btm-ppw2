<header>
    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        signOut(){
            console.log('sair');
            this.dispatch({
                type: "LOGOUT",
                payload: {
                    email: sessionStorage.getItem("btm-user-email"), user_token: sessionStorage.getItem("btm-user-token")
                }
            });
        }

        this.on('mount', function() {
            if(sessionStorage.getItem("btm-user-email") != "" && sessionStorage.getItem("btm-user-token") != "" && sessionStorage.getItem("btm-user-email") != undefined && sessionStorage.getItem("btm-user-token") != undefined){
                this.dispatch({
                    type: "SET_USERDATA",
                    payload: {
                        name: sessionStorage.getItem("btm-user-name"), email: sessionStorage.getItem("btm-user-email"), user_token: sessionStorage.getItem("btm-user-token")
                    }
                });


                try{
                    this.dispatch({
                        type: "GET_GROUPS", 
                        token: this.state.userReducer.user_data.user_token
                    });

                    this.dispatch({
                        type: "GET_GROUP_INVITATIONS", 
                        payload: { data: { token: this.state.userReducer.user_data.user_token} }
                    });
                    
                    /*console.log(this.state.dashboardReducer.groups[0].id)
                    if(this.state.dashboardReducer.groups.length > 0 && this.state.dashboardReducer.groups[0].id != "-"){
                       this.dispatch({
                            type: "SET_CURRENT_GROUP_ID", 
                            payload: this.state.dashboardReducer.groups[0].id
                        });
                        console.log(this.state.dashboardReducer.groups[0].id);
                        if(this.state.dashboardReducer.current_group_id){
                            console.log('aqui***');
                            this.dispatch({
                                type: "GET_GROUP", 
                                payload: { payload: { data: { group_id: this.state.dashboardReducer.groups[0].id, token: this.state.userReducer.user_data.user_token} } }
                            });
                        }
                        
                    }   */
                }catch(err){
                    console.log('erro no header.tag');
                    console.log(err.response)
                }
            }
        });
    </script>
    <style>
        #dashboard-menu .nav-link{
            cursor: pointer;
            color: #192da5;
            font-weight: bold;
        }
        nav{
            background: #35e07a;
        }
    </style>
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <a class="navbar-brand" href="#/dashboard"><img id="logobtm" src="imgs/logo.png"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#dashboard-menu" aria-controls="dashboard-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="dashboard-menu">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item" if={this.state.userReducer.user_data.email == ""}>
                        <a class="nav-link" href="#/home">Home</a>
                    </li>
                    <li class="nav-item" if={this.state.userReducer.user_data.email == ""}>
                        <a class="nav-link" href="#components">Funcionalidades</a>
                    </li>
                    <li class="nav-item" if={this.state.userReducer.user_data.email == ""}>
                        <a class="nav-link" data-toggle="modal" data-target="#login">Entrar</a>
                    </li>
                    <li class="nav-item" if={this.state.userReducer.user_data.email == ""}>
                        <a class="nav-link" data-toggle="modal" data-target="#register">Cadastre-se</a>
                    </li>
                    <li class="nav-item" if={this.state.userReducer.user_data.email != ""}>
                        <a class="nav-link" href="/#/dashboard">Painel</a>
                    </li>
                    <li class="nav-item" if={this.state.userReducer.user_data.email != ""}>
                        <a class="nav-link" href="#/anotacoes" class="btn">Relatorios</a>
                    </li>
                    <li class="nav-item" if={this.state.userReducer.user_data.email != ""}>
                        <a class="nav-link" data-toggle="modal" data-target="#notifications">Notificações {this.state.dashboardReducer.group_invitations.length}</a>
                    </li>
                    <li class="nav-item" if={this.state.userReducer.user_data.email != ""}>
                        <a class="nav-link" href="#/editarUsuario" data-toggle="modal" data-target="#editUser">Minha conta</a>
                    </li>
                    <li class="nav-item" if={this.state.userReducer.user_data.email != ""}>
                        <a class="nav-link" onclick={signOut}>Sair</a>
                    </li>
                </ul>           
            </div>
        </div>
    </nav>
</header>