<addNote>
    <div class="modal fade" id="addNote">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Nova Nota</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label for="new_note_name">Titulo da nota</label>
                    <p><input type="text" class="form-control" id="new_note_name" placeholder="Digite um titulo para a nota."></p> 
                    <label for="new_note_content">Descrição: </label>
                    <p><textarea id="new_note_content" name="description" class="form-control" placeholder="Descrição da nota."></textarea></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick={newNote}>Criar Nota</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)
        
        newNote(){
            let note = { name: document.getElementById('new_note_name').value, content: document.getElementById('new_note_content').value };
            let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id }
            let group = { id: this.state.dashboardReducer.current_group_id };
            
            console.log(note);
            this.dispatch({
                type: "ADD_NOTE",
                payload: {
                    note,
                    group,
                    userinfo
                }
            });
            $('#note').modal('show');
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#addNote').modal('hide');
            }
        })
    </script>
    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 7%;
        }
    </style>
</addNote>