<editTask>
    <div class="modal fade" id="editTask">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}>
                    <img src="imgs/spinner.svg">
                </div>
                <div class="modal-header">
                    <h4 class="modal-title">Editar Tarefa</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label for="edit_task_title">Titulo da tarefa</label>
                    <p>
                        <input type="text" class="form-control" id="edit_task_title" value="{opts.name}"
                            placeholder="{opts.name}">
                    </p>
                    <label for="edit_task_description">Descrição: </label>
                    <p>
                        <textarea id="edit_task_description" name="description" class="form-control" value="{opts.description}"
                            placeholder="{opts.description}"></textarea>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick={deletarTask}>Excluir Tarefa</button>
                    <button type="button" class="btn btn-success" onclick={editarTask}>Editar Tarefa</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")    
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        editarTask(){
            let task = { id: opts.id, name: document.getElementById('edit_task_title').value, description: document.getElementById('edit_task_description').value, private: false };
            let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id };
            let group = { id: this.state.dashboardReducer.current_group_id };
            let category = { id: opts.category_id };
            
            this.dispatch({
                type: 'EDIT_TASK',
                payload: {
                    task,
                    userinfo,
                    group,
                    category
                }
            });
        }

        deletarTask(){
            let task = { id: opts.id }
            let userinfo = { token: this.state.userReducer.user_data.user_token, id: this.state.userReducer.user_data.id };
            let group = { id: this.state.dashboardReducer.current_group_id };
            let category = { id: opts.category_id };

            this.dispatch({
                type: 'DELETE_TASK',
                payload: {
                    task,
                    userinfo,
                    group,
                    category
                }
            });
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#editTask').modal('hide');
            }
        })
    </script>
</editTask>