<register>
    <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="cadastroTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="spinner" if={this.state.exibicaoReducer.showSpinner}><img src="imgs/spinner.svg"></div>
                <div class="alert alert-danger alert-dismissible fade show" role="alert" if={this.state.exibicaoReducer.showError}>
                    <strong>Ops!</strong> {this.state.exibicaoReducer.msgError}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="cadastroTitle">Cadastro</h5>
                </div>
                <div class="modal-body">
                    <form>
                        <p><input autofocus="autofocus" value="" name="usuario[nome]" id="register_name" type="text" placeholder="Nome" class="form-control" required="required"></p>
                        <p><input autofocus="autofocus" value="" name="usuario[email]" id="register_email" type="email" placeholder="E-mail" class="form-control" required="required"></p>
                        <p><input autocomplete="off" name="user[password]" id="register_password" type="password" placeholder="Senha" class="form-control" required="required"></p>
                        <p><input autocomplete="off" name="user[password]" id="register_confirmation_password" type="password" placeholder="Confirme sua senha" class="form-control" required="required"></p>
                    </form>
                </div>
                <div class="modal-footer">
                    <input name="commit" value="Cadastrar" class="btn btn-success btn-fill btn-block" data-disable-with="Cadastrar-se" type="button" onclick={this.register}>
                </div>
            </div>
        </div>
    </div>

    <script>
        this.mixin("state")
        this.mixin("reduxGlobal")
        this.mixin('subscribeStateTag')
        this.subscribeStateTag(this)

        register(){
            const password = document.getElementById('register_password').value;
            const confirmation_password = document.getElementById('register_confirmation_password').value;

            if(password == confirmation_password){
                let newUser = {name: (document.getElementById('register_name').value), email: (document.getElementById('register_email').value), password: password};
                console.log(newUser);
                this.dispatch({
                    type: "REGISTER_USER",
                    payload: {
                        newUser
                    }
                });
            }else{
                this.dispatch({
                    type: 'SHOW_ERROR'
                });
                this.dispatch({
                   type: 'MSG_ERROR', payload: "A senha de confirmação está incorreta."
                });
            }
        }

        this.on('update', function() {
            if(this.state.userReducer.closeModal){
                $('#register').modal('hide');
            }
        })
    </script>
    <style>
        #spinner{
            position: absolute;
            width: 100%;
            z-index: 10;
            text-align: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            padding-top: 15%;
        }
    </style>
</register>