import handleRoute from "./routes";
import catchErrors from "./error-handler";
import * as sagasAux from "./sagas-auxiliares";

/* Essa função é um observador, quando é feito um dispatch, o mesmo aciona o rootsaga e verifica qual yield 
será executado através da comparação do type enviado com o primeiro parâmetro do yield. */

export default function* rootSaga() {
    yield takeEvery("ROUTE", catchErrors, handleRoute);

    yield takeEvery("LOGIN", sagasAux.login);
    yield takeEvery("LOGOUT", sagasAux.logOut);
    yield takeEvery("REGISTER_USER", sagasAux.register);
    yield takeEvery("UPDATE_USER", sagasAux.updateUser);
    
    yield takeEvery("ADD_GROUP", sagasAux.addGroup);
    yield takeEvery("EDIT_GROUP", sagasAux.editGroup);
    yield takeEvery("DELETE_GROUP", sagasAux.deleteGroup);
    yield takeEvery("GET_GROUPS", sagasAux.getGroups);

    yield takeEvery("ADD_CATEGORY", sagasAux.addCategory);
    yield takeEvery("EDIT_CATEGORY", sagasAux.editCategory);
    yield takeEvery("DELETE_CATEGORY", sagasAux.deleteCategory);
    yield takeEvery("GET_CATEGORIES", sagasAux.getCategories);

    yield takeEvery("ADD_NOTE", sagasAux.addNote);
    yield takeEvery("EDIT_NOTE", sagasAux.editNote);
    yield takeEvery("DELETE_NOTE", sagasAux.deleteNote);
    yield takeEvery("GET_NOTES", sagasAux.getNotes);

    yield takeEvery("ADD_TASK", sagasAux.addTask);
    yield takeEvery("EDIT_TASK", sagasAux.editTask);
    yield takeEvery("DELETE_TASK", sagasAux.deleteTask);
    /* yield takeEvery("GET_TASKS", sagasAux.getTasks); */

    yield takeEvery("ADD_MEMBER_TO_GROUP", sagasAux.addMemberToGroup);
    
    yield takeEvery("GET_GROUP", sagasAux.getGroup);

    yield takeEvery("ACCEPT_INVITE", sagasAux.acceptInvite);
    yield takeEvery("REJECT_INVITE", sagasAux.rejectInvite);

    yield takeEvery("GET_GROUP_INVITATIONS", sagasAux.getGroupInvitations);
    yield takeEvery("GET_GROUP_MEMBERS", sagasAux.getGroupMembers);
    yield takeEvery("LEAVE_GROUP", sagasAux.leaveGroup);
    yield takeEvery("REMOVE_MEMBER_GROUP", sagasAux.removeMemberGroup);
    
}